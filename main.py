from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta


day_of_month = 25

start_date = datetime.now()
start_date = start_date.replace(day=25, hour=0, minute=0, second=0, microsecond=0)

total = 0
modified = 0
for i in range(12 * 5):
    date = start_date + relativedelta(months=i)
    j = date.weekday()
    # if Fri, Sat, or Sun
    if j > 3:
        date = date - timedelta(days=j - 3)

        modified += 1
    total += 1
    print(date.strftime("%A, %-m/%-d/%Y"))
    # print(date.weekday())

print(f"{total=} {modified=} {modified/total=}")
